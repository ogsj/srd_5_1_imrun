
## The Imrun Pantheon

The Imrun pantheon consists of former heroes ascended to divine stature - the Imruní. There are eight greater gods and nine lesser gods. Alongside these gods there are several spirits (or demons) worshipped in different parts of the world. Above them all stand Marghû the first king that turned evil and wicked. There are also older gods and forgotten deities that were before the current gods.

**Table- Imrun Deities**

| Deity                                             | Alignment | Suggested Domains  | Symbol                             |
|---------------------------------------------------|-----------|--------------------|------------------------------------|
| **Greater gods**||||
| Marghû, the great enemy                           | CE        | Undead, war, deceit|                                    |
| Fréma, the god of harvest                         | NE        | Life               |Two crossed flails|
| Emra, goddess of sorrow            | NG        | Light             |A stylized black tear|
| Melfenos, the trickster, god of chimneys        | NG        | Life              |A feather, or curling wisp of smoke|
| Nár, the god of war            | LG        | Life              ||
| Simner, the lord of storms               | N         | Nature            |A lightning bolt|
| Skéa, god of knowledge                            | NG        | Knowledge, Life   ||
| Vána, goddess of life and healing                 | CN        | Knowledge, Life   ||
| Ulahine, the goddes of the wild                   | LN        | Nature, Tempest   ||
| **Lesser gods**||||
| Aedh, steward of the dead                       | NE        | Knowledge         ||
| Forgenninn, god of hunters                       | CE        | War               |A bow and arrow|
| Hlíra, the godess of sea-farers                    | N         | War               ||
| Hrysta, the godess of wisdom                  | NG        | Knowledge         ||
| Kunrid, the godess of love               | N         | Nature            ||
| Lekka, the godess of the hearth               | N         | Nature            ||
| Lorýn, the pathfinder godess               | N         | Nature            ||
| Midril, the god of blacksmiths               | N         | Nature            ||
| Myntea, the god of commerce               | N         | Nature            ||
| Sann, the god of construction and carpentry               | N         | Nature            ||
|                                                   |           |                   |                                    |
